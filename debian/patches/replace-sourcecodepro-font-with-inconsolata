Description: Remove hard-coded dependency on the Source Code Pro font
 Mu bundles the Adobe Source Code Pro font with its source, and hard-codes its
 use within the editor. This patch removes this requirement, replacing it with
 the Inconsolata font, which is already packaged for Debian. Separately, we
 repackage the source (via Files-Excluded) to remove the fonts from the
 Debian source tarball and add a binary dependency on fonts-inconsolata.
 .
 Note that Debian packaging efforts for the Adobe Font Development Kit for
 OpenType (#762252) and the Source Code Pro fonts (#736681) remain unresolved
 at the time of creating this patch.
Author: Nick Morrott <knowledgejunkie@gmail.com>
Forwarded: not-needed
Last-Update: 2018-12-21
---
--- a/mu/interface/themes.py
+++ b/mu/interface/themes.py
@@ -50,10 +50,9 @@
     logger.warn("Overriding built-in editor font due to Issue #552")
     FONT_NAME = "Monaco"
 else:  # pragma: no cover
-    FONT_NAME = "Source Code Pro"
+    FONT_NAME = "Inconsolata"
 
-FONT_FILENAME_PATTERN = "SourceCodePro-{variant}.otf"
-FONT_VARIANTS = ("Bold", "BoldIt", "It", "Regular", "Semibold", "SemiboldIt")
+FONT_FILENAME = "Inconsolata.otf"
 # Load the two themes from resources/css/[night|day].css
 # NIGHT_STYLE is a dark theme.
 NIGHT_STYLE = load_stylesheet('night.css')
@@ -89,10 +88,9 @@
         """
         if cls._DATABASE is None:
             cls._DATABASE = QFontDatabase()
-            for variant in FONT_VARIANTS:
-                filename = FONT_FILENAME_PATTERN.format(variant=variant)
-                font_data = load_font_data(filename)
-                cls._DATABASE.addApplicationFontFromData(font_data)
+            filename = FONT_FILENAME
+            font_data = load_font_data(filename)
+            cls._DATABASE.addApplicationFontFromData(font_data)
         return cls._DATABASE
 
     def load(self, size=DEFAULT_FONT_SIZE):
--- a/MANIFEST.in
+++ b/MANIFEST.in
@@ -5,7 +5,6 @@
 include conf/*
 include mu/resources/css/*
 include mu/resources/images/*
-include mu/resources/fonts/*
 include mu/resources/pygamezero/*
 include run.py
 recursive-include mu/locale *
